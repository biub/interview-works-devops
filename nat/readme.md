用于第三方回调如支付开发时需要在内网进行调试，使用自己的内网穿透调试可以使用自己的域名，使用已经配好的https

1. 阿里云配置域名到公网服务器,将要转发的端口在公网服务器nginx配置好，如 nginx application port -> ssh remote port 

2. 新建ssh用户 your_user，禁用该用户的shell,sftp权限

3. 利用supervisor启动ssh客户端，并开启port forward,supervisor示例
ssh -l natuser -i "/www/wwwroot/nat.private.com/your_ssh_id_rsa" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -N -R your_remote_port:127.0.0.1:80 your_remote_server_host

4.  最后，在测试服nginx配置把请求对接到末端应用就行