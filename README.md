# 面试作品-Devops

#### 介绍
开始一个基于git+jenkin部署到k8s的应用用于面试时使用。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0416/122534_cbc00901_1311864.png "屏幕截图.png")

#### runner安装教程

1.  从gitlab管理面板-runners中获取register token
2.  配置doker\gitlab-runner\Dockerfile，build并推送到私有harbor
3.  修改gitlab-runner\gitlab-runner-mvn.yml在的image 为自己定制的image
4.  kubectl apply -f gitlab-runner-mvn.yml 发布runner

#### ci使用说明

1.  在jenkins中新建一个项目，在参数化构建过程中添加一个字符串参数image_version用buildwithparam方式接收
2.  构建触发器中开启触发远程构建，生成并填入自己的jenkins token
3.  jenkins安装ssh插件,并配置好ssh连接信息
4.  在构建在选择在远程主机执行，填入kubectl set image deploy your-k8s-application demo-container=harbor.private.com/demo/demo:$image_version
5.  在项目根目录新建一个.gitlab-ci.yml,如demo mvn-ci中修改tag为runner注册的tag,这里举例是用文件数字自增来控制版本号的，默认是没有文件的，所以需要改成自己的版本控制方式
6.  使用新的版本号推送镜像到私有harbor
7.  将版本号发送到jenkins中去构建，这里使用的是curl+jenkins api主动构建,需要runner image安装curl，可以根据情况自己选择jenkins构建触发方式